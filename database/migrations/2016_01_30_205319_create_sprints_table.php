<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sprints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('release_id')->unsigned();
            $table->integer('number');
            $table->string('name');
            $table->string('objetive');
            $table->integer('days');
            $table->float('ideal_work')->nullable();
            $table->date('start_date');
            $table->date('finish_date')->nullable();
            $table->timestamps();

            $table->foreign('release_id')->references('id')->on('releases')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sprints');
    }
}
