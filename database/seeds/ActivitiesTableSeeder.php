<?php

use Illuminate\Database\Seeder;
use App\Project;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$faker = Faker\Factory::create();
        $projects = Project::get();
        $hours = [2, 4, 6, 8, 10];

        foreach ($projects as $project) {
        	$developers = $project->developers->toArray();
        	$releases = $project->releases;

        	foreach ($releases as $release) {
        		$stories = $release->userStories->toArray();
        		$sprints = $release->sprints;

        		foreach ($sprints as $sprint) {
        			$developer = $faker->randomElement($developers);
        			$story = $faker->randomElement($stories);

        			if ($sprint->days > 10) {
                        $count = 10;
                    } else {
                        $count = 4;
                    }

                    for ($i=0; $i < $count; $i++) {
	        			$sprint->activities()->create([
	        				'user_id' => $developer['id'],
							'user_story_id' => $story['id'],
							'status' => $faker->numberBetween(1, 3),
							'description' => $faker->sentence(10),
	        				'estimated_hours' => $faker->randomElement($hours)
		        		]);
		        	}

                    $ideal_work = $sprint->activities()->sum('estimated_hours');
                    $ideal_work = $ideal_work / $sprint->days;
                    $sprint->ideal_work = $ideal_work;
                    $sprint->save();
        		}
       		}
        }

    }
}
