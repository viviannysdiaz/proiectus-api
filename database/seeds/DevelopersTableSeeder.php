<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Project;

class DevelopersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $projects_id = Project::lists('id')->toArray();
        $developers_ids = User::whereHas('roles' , function($q){ $q->where('slug', 'developer');})->lists('id')->toArray();

        foreach ($projects_id as $index) {
        	$project = Project::find($index);
        	for ($p=0; $p < 3; $p++) {
				$id = $faker->unique()->randomElement($developers_ids);
	        	$developer = $project->developers()->attach($id);
	        }
        }
    }
}
