<?php

use Illuminate\Database\Seeder;
use App\Sprint;

class RemainingWorksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sprints = Sprint::get();
        $hours = [0, 2, 4, 6, 8, 10];
        $faker = Faker\Factory::create();

        foreach ($sprints as $sprint) {
        	$activities = $sprint->activities;
        	for ($i=1; $i<=$sprint->days; $i++) {
        		foreach ($activities as $activity) {
        			$activity->remainingWorks()->create([
        				'day' => $i,
        				'remaining_work' => $faker->randomElement($hours)
        			]);
        		}
        	}
        }
    }
}
