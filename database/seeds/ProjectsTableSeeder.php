<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
		$onwers_ids = App\User::whereHas('roles' , function($q){ $q->where('slug', 'owner');})->lists('id')->toArray();
		$scrums_ids = App\User::whereHas('roles' , function($q){ $q->where('slug', 'scrum.master');})->lists('id')->toArray();
		$users_ids = App\User::lists('id')->toArray();

		$project = Project::create([
				'product_owner_id' => $faker->randomElement($onwers_ids),
		        'scrum_master_id' => 3,
		        'name' => 'Project Default',
		        'image' => $faker->imageUrl(640, 480),
		        'description' => $faker->sentence(10),
		        'start_date' => $faker->date(),
            	'finish_date' => $faker->date()
	    ]);

		for ($i=0; $i < 5; $i++) {
			$project = Project::create([
				'product_owner_id' => $faker->randomElement($onwers_ids),
		        'scrum_master_id' => $faker->randomElement($scrums_ids),
		        'name' => $faker->company,
		        'image' => $faker->imageUrl(640, 480),
		        'description' => $faker->sentence(10),
		        'start_date' => $faker->date(),
            	'finish_date' => $faker->date()
	        ]);
		}
	}
}
