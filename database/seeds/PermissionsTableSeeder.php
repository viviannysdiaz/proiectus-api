<?php

use Johnnymn\Sim\Roles\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Creation of Permissions.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Allows User to manage the "Users"
         */
        Permission::create([
            'name' => 'Create users',
            'slug' => 'create.users',
            'description' => 'Allows User to create users'
        ]);

        Permission::create([
            'name' => 'Update users',
            'slug' => 'update.users',
            'description' => 'Allows User to update users'
        ]);

        Permission::create([
            'name' => 'List users',
            'slug' => 'list.users',
            'description' => 'Allows User to list users'
        ]);

        Permission::create([
            'name' => 'Delete users',
            'slug' => 'delete.users',
            'description' => 'Allows User to delete users'
        ]);

        /*
         * Allows User to manage the "Projects"
         */
        Permission::create([
            'name' => 'Create projects',
            'slug' => 'create.projects',
            'description' => 'Allows User to create projects'
        ]);

        Permission::create([
            'name' => 'Update projects',
            'slug' => 'update.projects',
            'description' => 'Allows User to update projects'
        ]);

        Permission::create([
            'name' => 'List projects',
            'slug' => 'list.projects',
            'description' => 'Allows User to list projects'
        ]);

        Permission::create([
            'name' => 'Delete projects',
            'slug' => 'delete.projects',
            'description' => 'Allows User to delete projects'
        ]);

        /*
         * Allows User to manage the "User Stories"
         */
        Permission::create([
            'name' => 'Create user.stories',
            'slug' => 'create.user.stories',
            'description' => 'Allows User to create user stories'
        ]);

        Permission::create([
            'name' => 'Update user.stories',
            'slug' => 'update.user.stories',
            'description' => 'Allows User to update user stories'
        ]);

        Permission::create([
            'name' => 'List user.stories',
            'slug' => 'list.user.stories',
            'description' => 'Allows User to list user stories'
        ]);

        Permission::create([
            'name' => 'Delete user.stories',
            'slug' => 'delete.user.stories',
            'description' => 'Allows User to delete user stories'
        ]);

        /*
         * Allows User to manage the "Sprints"
         */
        Permission::create([
            'name' => 'Create releases',
            'slug' => 'create.releases',
            'description' => 'Allows User to create releases'
        ]);

        Permission::create([
            'name' => 'Update releases',
            'slug' => 'update.releases',
            'description' => 'Allows User to update releases'
        ]);

        Permission::create([
            'name' => 'List releases',
            'slug' => 'list.releases',
            'description' => 'Allows User to list releases'
        ]);

        Permission::create([
            'name' => 'Delete releases',
            'slug' => 'delete.releases',
            'description' => 'Allows User to delete releases'
        ]);

        /*
         * Allows User to manage the "Sprints"
         */
        Permission::create([
            'name' => 'Create sprints',
            'slug' => 'create.sprints',
            'description' => 'Allows User to create sprints'
        ]);

        Permission::create([
            'name' => 'Update sprints',
            'slug' => 'update.sprints',
            'description' => 'Allows User to update sprints'
        ]);

        Permission::create([
            'name' => 'List sprints',
            'slug' => 'list.sprints',
            'description' => 'Allows User to list sprints'
        ]);

        Permission::create([
            'name' => 'Delete sprints',
            'slug' => 'delete.sprints',
            'description' => 'Allows User to delete sprints'
        ]);

        /*
         * Allows User to manage the "Sprint Backlog"
         */
        Permission::create([
            'name' => 'Create sprint backlog',
            'slug' => 'create.sprintBacklog',
            'description' => 'Allows User to create sprintBacklog'
        ]);

        Permission::create([
            'name' => 'Update sprint backlog',
            'slug' => 'update.sprintBacklog',
            'description' => 'Allows User to update sprintBacklog'
        ]);

        Permission::create([
            'name' => 'List sprint backlog',
            'slug' => 'list.sprintBacklog',
            'description' => 'Allows User to list sprintBacklog'
        ]);

        Permission::create([
            'name' => 'Delete sprint backlog',
            'slug' => 'delete.sprintBacklog',
            'description' => 'Allows User to delete sprintBacklog'
        ]);

        /*
         * Allows User to manage the "Developer Teams"
         */
        Permission::create([
            'name' => 'Create developer teams',
            'slug' => 'create.teams',
            'description' => 'Allows User to create developer teams'
        ]);

        Permission::create([
            'name' => 'Update developer teams',
            'slug' => 'update.teams',
            'description' => 'Allows User to update developer teams'
        ]);

        Permission::create([
            'name' => 'List developer teams',
            'slug' => 'list.teams',
            'description' => 'Allows User to list developer teams'
        ]);

        Permission::create([
            'name' => 'Delete developer teams',
            'slug' => 'delete.teams',
            'description' => 'Allows User to delete developer teams'
        ]);

        /*
         * Allows User to manage the "Developers"
         */
        Permission::create([
            'name' => 'Create developers',
            'slug' => 'create.developers',
            'description' => 'Allows User to create developers'
        ]);

        Permission::create([
            'name' => 'Update developers',
            'slug' => 'update.developers',
            'description' => 'Allows User to update developers'
        ]);

        Permission::create([
            'name' => 'List developers',
            'slug' => 'list.developers',
            'description' => 'Allows User to list developers'
        ]);

        Permission::create([
            'name' => 'Delete developers',
            'slug' => 'delete.developers',
            'description' => 'Allows User to delete developers'
        ]);
    }
}
