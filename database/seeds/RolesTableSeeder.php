<?php

use Johnnymn\Sim\Roles\Models\Role;
use Johnnymn\Sim\Roles\Models\Permission;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Creation of Roles.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'Superuser',
            'slug' => 'superuser',
            'description' => 'The user with highest privilegies on the system',
            'level' => 10
        ]);

        $permissions = Permission::all();
        $permissions->each(function ($item, $key) use ($role) {
            $role->attachPermission($item);
        });

        $roleMaster = Role::create([
            'name' => 'Scrum Master',
            'slug' => 'scrum.master',
            'description' => 'Project Scrum master',
            'level' => 8
        ]);

        $permission = Permission::where('slug', 'create.projects')->first();
        $roleMaster->attachPermission($permission);

        $permission = Permission::where('slug', 'list.projects')->first();
        $roleMaster->attachPermission($permission);

        $permission = Permission::where('slug', 'update.projects')->first();
        $roleMaster->attachPermission($permission);

        $roleDeveloper = Role::create([
            'name' => 'Developer',
            'slug' => 'developer',
            'description' => 'Project developer',
            'level' => 8
        ]);

        $roleOwner = Role::create([
            'name' => 'Product owner',
            'slug' => 'owner',
            'description' => 'Project owner',
            'level' => 8
        ]);
    }
}
