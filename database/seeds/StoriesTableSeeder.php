<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Release;
use App\UserStory;

class StoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$faker = Faker\Factory::create();
        $projects_id = Project::lists('id')->toArray();

        foreach ($projects_id as $index) {
        	$project = Project::find($index);
        	$releases_ids = $project->releases()->lists('id')->toArray();

			foreach ($releases_ids as $releases_id) {
	        	$release = Release::find($releases_id);
	        	for ($i=0; $i < 5; $i++) {
					$project->userStories()->create([
	        	 		'release_id' => $release->id,
		      			'description' => $faker->sentence(10)
		        	]);
		        }
		    }
        }
    }
}
