<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Release;

class ReleasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $projects= Project::get();

        foreach ($projects as $project) {
        	for ($i=1; $i<=5; $i++) {
				$project->releases()->create([
	        		'number' => $i,
                    'objetive' => $faker->sentence(3),
	        		'description' => $faker->sentence(10)
	        	]);
	        }
        }
    }
}
