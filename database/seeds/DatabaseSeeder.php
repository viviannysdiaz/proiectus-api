<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(DevelopersTableSeeder::class);
        $this->call(ReleasesTableSeeder::class);
        $this->call(StoriesTableSeeder::class);
        $this->call(SprintsTableSeeder::class);
        $this->call(ActivitiesTableSeeder::class);
        $this->call(RemainingWorksTableSeeder::class);

        Model::reguard();
    }
}
