<?php

use Illuminate\Database\Seeder;
use App\Release;

class SprintsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $releases = Release::get();
        $days = [10, 16, 20];

        foreach ($releases as $release) {
        	for ($i=1; $i <= 5; $i++) {
				$release->sprints()->create([
        			'number' => $i,
                    'name' => $faker->sentence(3),
        			'objetive' => $faker->sentence(10),
        			'start_date' => $faker->dateTimeThisMonth('now'),
        			'days' => $faker->randomElement($days)
        		]);
	        }
        }
    }
}
