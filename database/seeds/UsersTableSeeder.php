<?php

use Johnnymn\Sim\Roles\Models\Role;
use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $user = User::create([
        	'name' => 'Super User',
        	'username' => 'superuser',
            'avatar' => 'https://randomuser.me/api/portraits/med/men/81.jpg',
        	'email' => 'super.user@proiectus.com',
        	'password' => '123456',
        ]);

        $super_role = Role::where('slug', 'superuser')->first();
        $user->attachRole($super_role);

        $masterUser = User::create([
            'name' => 'Scrum Master',
            'username' => 'scrum.master',
            'avatar' => 'https://randomuser.me/api/portraits/med/men/81.jpg',
            'email' => 'scrum.master@proiectus.com',
            'password' => '123456',
        ]);

        $master_role = Role::where('slug', 'scrum.master')->first();
        $masterUser->attachRole($master_role);

        $developer = Role::where('slug', 'developer')->first();
        $owner = Role::where('slug', 'owner')->first();
        $master_role = Role::where('slug', 'scrum.master')->first();

        $faker = Faker\Factory::create();

        $vivi = User::create([
            'name' => 'Viviannys Díaz',
            'username' => 'viviannysdiaz',
            'avatar' => 'https://randomuser.me/api/portraits/med/women/91.jpg',
            'email' => 'viviannysd@manzanares.com.ve',
            'password' => '123456',
        ]);

        $vivi->attachRole($master_role);
        $vivi->attachRole($developer);

        for ($i=0; $i<20 ; $i++) {
            $user = User::create([
                'name' => $faker->name,
                'username' => $faker->unique()->userName,
                'avatar' => $faker->imageUrl(640, 480),
                'email' => $faker->unique()->freeEmail,
                'password' => '123456'
            ]);

            $user->attachRole($developer);
        }

        for ($i=0; $i<5 ; $i++) {
            $user = User::create([
                'name' => $faker->name,
                'username' => $faker->unique()->userName,
                'avatar' => $faker->imageUrl(640, 480),
                'email' => $faker->unique()->freeEmail,
                'password' => '123456'
            ]);

            $user->attachRole($owner);
        }

        for ($i=0; $i<5 ; $i++) {
            $user = User::create([
                'name' => $faker->name,
                'username' => $faker->unique()->userName,
                'avatar' => $faker->imageUrl(640, 480),
                'email' => $faker->unique()->freeEmail,
                'password' => '123456'
            ]);

            $user->attachRole($master_role);
        }
    }
}

