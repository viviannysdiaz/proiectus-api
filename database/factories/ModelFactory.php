<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'avatar' => $faker->imageUrl(640, 480),
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->freeEmail,
        'password' => '123456'
    ];
});

$factory->define(App\Project::class, function (Faker\Generator $faker) {
    $users_ids = App\User::lists('id')->toArray();
    return [
        'product_owner_id' => $faker->randomElement($users_ids),
        'scrum_master_id' => $faker->randomElement($users_ids),
        'name' => $faker->company,
        'image' => $faker->imageUrl(640, 480),
        'description' => $faker->sentence(10, true)
    ];
});

$factory->define(App\UserStory::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->sentence(10, true)
    ];
});

$factory->define(App\Release::class, function (Faker\Generator $faker) {
    return [
    	'description' => $faker->sentence(10, true)
    ];
});
