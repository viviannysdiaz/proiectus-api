<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_owner_id',
        'scrum_master_id',
        'name',
        'image',
        'finished',
        'description',
        'start_date',
        'finish_date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'current_sprint',
    ];

    /**
     * Relationships.
     *
     */
    public function userStories()
    {
        return $this->hasMany('App\UserStory');
    }

    public function releases()
    {
        return $this->hasMany('App\Release');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'product_owner_id');
    }

    public function scrumMaster()
    {
        return $this->belongsTo('App\User', 'scrum_master_id');
    }

    public function developers()
    {
        return $this->belongsToMany('App\User', 'developers');
    }

    public function sprints()
    {
        return $this->hasManyThrough('App\Sprint', 'App\Release');
    }

    /**
     * Accesors
     *
     */
    public function getCurrentSprintAttribute()
    {
        return $this->sprints()->orderBy('id', 'DESC')->first();
    }
}
