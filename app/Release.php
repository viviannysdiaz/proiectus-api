<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Release extends Model
{
  	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'releases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'number',
        'objetive',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Relationships.
     *
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function userStories()
    {
        return $this->hasMany('App\UserStory');
    }

    public function sprints()
    {
        return $this->hasMany('App\Sprint');
    }
}
