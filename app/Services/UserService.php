<?php

namespace App\Services;

use JWTAuth;
use App\Helpers\Error;
use App\Repositories\UserRepository;

class UserService extends Service
{
	public function __construct()
	{
		$this->repository = new UserRepository();

		$this->addRequiredPermission('registerUser', ['create.users']);
	}

	/**
	 * Authenticate an user in the system.
	 * @param  array $data
	 * @return App\User
	 */
	protected function authenticateUser($data)
	{
		$response = $this->repository->authenticateUser($data);
		if ($response instanceof Error) {
			return $response;
		}
		return $this->handleLoginUser($response);
	}

	/**
	 * Register a new user on the system.
	 * @param  App\User $user
	 * @param  array $data
	 * @return App\User
	 */
	protected function registerUser($user, $data)
	{
		return $this->repository->create($data);
	}

	/**
     * Create a new token for the user.
     * @param  App\User $user
     * @return array
     */
    protected function refreshToken($user)
    {
       	return $this->handleLoginUser($user);
    }

    /**
     * Create the token after the user authentication.
     * @param  App\User $user
     * @return array
     */
    private function handleLoginUser($user)
    {
        $token = JWTAuth::fromUser($user);
        $roles = $this->repository->getUserRolesByLevel($user);
        return [
            'token' => $token,
            'ttl' => config('jwt.ttl'),
            'user' => $user,
            'roles' => $roles
        ];
    }

    protected function getUsersList($user, $limit)
    {
        //if ($user->isSuperuser()) {
            return $this->repository->getUsersList($limit);
        /*} else {
            return $this->repository->getUserProjects($user, $limit);
        }*/
    }

}
