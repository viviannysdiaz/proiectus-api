<?php

namespace App\Services;

use JWTAuth;
use App\Helpers\Error;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;

class ProjectService extends Service
{
	public function __construct()
	{
		$this->repository = new ProjectRepository();
		$this->userRepository = new UserRepository();

		$this->addRequiredPermission('listProject', ['list.projects']);
		$this->addRequiredPermission('createProject', ['create.projects']);
		$this->addRequiredPermission('showProject', ['list.projects']);
		$this->addRequiredPermission('updateProject', ['update.projects']);
		$this->addRequiredPermission('deleteProject', ['delete.projects']);
	}

	/**
	 * Allows the user to list Projects.
	 * @param  array $data
	 * @return App\User
	 */
	protected function listProject($user, $limit)
	{
		if ($user->isSuperuser()) {
			return $this->repository->getProjectList($limit);
		} else {
			return $this->repository->getUserProjects($user, $limit);
		}
	}

	/**
	 * Allows the user to create a new Project.
	 * @param  array $data
	 * @return App\User
	 */
	protected function createProject($user, $data)
	{
		return $this->repository->create($data);
	}

	/**
	 * Allows the user to view the information of a Project.
	 * @param  array $data
	 * @return App\User
	 */
	protected function showProject($user, $id)
	{
		return $this->repository->findById($id);
	}

	/**
	 * Allows the user to update the information of a Project.
	 * @param  array $data
	 * @return App\User
	 */
	protected function updateProject($user, $projectId, $data)
	{
		$project = $this->repository->findById($projectId);
		if ($project) {
			return $this->repository->update($project, $data);
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to delete the Project.
	 * @param  array $data
	 * @return App\User
	 */
	protected function deleteProject($user, $projectId)
	{
		$project = $this->repository->findById($projectId);
		if ($project) {
			return $this->repository->deleteById($projectId);
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to list the Projects that are associated to one user.
	 * @param  array $data
	 * @return App\User
	 */
	public function getUserProjectsByRole($user, $userId)
	{
        $user_project = $this->userRepository->findById($userId);

        if (!$user_project) {
        	return new Error('http-error', 'not-found',
				['user' => $userId], 'The resource was not found', 404);
        }
        $roles = $user_project->roles()->select(['roles.id', 'roles.slug'])->get();
        $projects = [];
        foreach ($roles as $key => $role) {
            if ($role->slug == 'owner') {
                $projects['Owner'] = $user_project->owner()
                	->select(['id', 'name'])->get();
            } elseif ($role->slug == 'scrum.master') {
                $projects['Scrum Master'] = $user_project->scrumMaster()
                	->select(['id', 'name'])->get();
            } elseif ($role->slug == 'developer') {
                $projects['Developer'] = $user_project->developers()
                	->select(['projects.id', 'projects.name'])->get();
            }
        }
        return $projects;
    }

    /**
     * [getCurrentActivities description]
     * @param  App\User $user
     * @param  int $projectId
     * @param  int $activity_status
     * @return App\Activity
     */
    public function getCurrentActivities($user, $projectId, $activity_status)
    {
    	$project = $this->repository->findById($projectId);
    	if (!$project) {
    		return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
    	}

    	return $this->repository->getCurrentActivities($project, $activity_status);
    }

}
