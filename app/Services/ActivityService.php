<?php

namespace App\Services;

use JWTAuth;
use App\Helpers\Error;
use App\Repositories\ProjectRepository;
use App\Repositories\ActivityRepository;
use App\Repositories\ReleaseRepository;

class ActivityService extends Service
{
	public function __construct()
	{
		$this->repository = new ActivityRepository();
		$this->projectRepository = new ProjectRepository();
		$this->releaseRepository = new ReleaseRepository();

		//$this->addRequiredPermission('listSprintActivities', ['list.activities']);

	}

	/**
	 * Allows the user to list the Activities that belong to a Sprint.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @return
	 */
	protected function listSprintActivities($user, $projectId, $releaseId, $sprintId, $status=null)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases()->find($releaseId);
			if ($release) {
				$sprint = $release->sprints()->find($sprintId);
				if ($sprint) {
					if ($status) {
						return $this->repository->getActivitiesByStatus($sprintId, $status);
					}
					else{
						return $sprint->activities;
					}
				} else {
					return new Error('http-error', 'not-found',
						['sprint' => $sprintId], 'The resource was not found', 404);
				}
			} else {
				return new Error('http-error', 'not-found',
					['release' => $releaseId], 'The resource was not found', 404);
			}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

}
