<?php

namespace App\Services;

use JWTAuth;
use App\Helpers\Error;
use App\Repositories\RemainingWorkRepository;
use App\Repositories\ActivityRepository;
use App\Repositories\SprintRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\ReleaseRepository;

class RemainingWorkService extends Service
{
	public function __construct()
	{
		$this->repository = new RemainingWorkRepository();
		$this->activityRepository = new ActivityRepository();
		$this->sprintRepository = new SprintRepository();
		$this->projectRepository = new ProjectRepository();
		$this->releaseRepository = new ReleaseRepository();
		//$this->addRequiredPermission('listSprintActivities', ['list.activities']);
	}

	protected function listRemainingWork($user, $activityId)
	{
		$activity = $this->activityRepository->findById($activityId);
		if ($activity) {
			return $activity->remainingWorks()->paginate();
		} else {
			return new Error('http-error', 'not-found',
				['activity' => $activity], 'The resource was not found', 404);
		}
	}

	protected function burnDownChartInfo($user, $projectId, $releaseId, $sprintId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
				if ($release) {
					$sprint = $release->sprints->find($sprintId);
					if ($sprint) {
						$rows['ideal_work'] = [];
						$rows['remaining_work'] = [];

						$activityIds = $sprint->activities()->lists('id');
						$total_work = $sprint->activities()->sum('estimated_hours');

						for ($i=1; $i<=$sprint->days; $i++) {
							$rows['ideal_work'][$i] =  $total_work - ($sprint->ideal_work * $i);
							$rows['remaining_work'][$i] = $this->repository->getRemainingWorkByDay($activityIds, $i);
						}
						return $rows;
					} else {
						return new Error('http-error', 'not-found',
							['sprint' => $sprintId], 'The resource was not found', 404);
					}
				} else {
					return new Error('http-error', 'not-found',
						['release' => $releaseId], 'The resource was not found', 404);
				}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}
}