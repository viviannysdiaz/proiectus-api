<?php

namespace App\Services;

use JWTAuth;
use App\Helpers\Error;
use App\Repositories\ProjectRepository;
use App\Repositories\UserStoryRepository;

class UserStoryService extends Service
{
	public function __construct()
	{
		$this->repository = new UserStoryRepository();
		$this->projectRepository = new ProjectRepository();

		$this->addRequiredPermission('listUserHistories', ['list.user.stories']);
		$this->addRequiredPermission('createUserStories', ['create.user.stories']);
		$this->addRequiredPermission('showUserStory', ['list.user.stories']);

	}

	/**
	 * Allows the user to list the User Stories that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @return
	 */
	protected function listUserStories($user, $projectId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			return $project->userStories()->paginate();
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to create the User Stories that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  array $data
	 * @return
	 */
	protected function createUserStory($user, $projectId, $data)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$data['project_id'] = $project->id;
			return $this->repository->create($data);
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to list the User Stories that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $userStoryId
	 * @return
	 */
	protected function showUserStory($user, $projectId, $userStoryId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$userStory = $project->userStories()->find($userStoryId);
			if ($userStory) {
				return $userStory;
			}
			else{
				return new Error('http-error', 'not-found',
					['userStory' => $userStoryId], 'The resource was not found', 404);
			}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to update the User Story that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $userStoryId
	 * @param  array $data
	 * @return
	 */
	protected function updateUserStory($user, $projectId, $userStoryId, $data)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$userStory = $project->userStories->find($userStoryId);
				if ($userStory) {
					return $this->repository->update($userStory, $data);
				} else {
					return new Error('http-error', 'not-found',
						['userStory' => $userStoryId], 'The resource was not found', 404);
				}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to delete the User Story that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $userStoryId
	 * @param  array $data
	 * @return
	 */
	protected function deleteUserStory($user, $projectId, $userStoryId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$userStory = $project->userStories->find($userStoryId);
				if ($userStory) {
					return $this->repository->deleteById($userStory->id);
				} else {
					return new Error('http-error', 'not-found',
						['userStory' => $userStoryId], 'The resource was not found', 404);
				}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

}
