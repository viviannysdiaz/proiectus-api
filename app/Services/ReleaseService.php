<?php

namespace App\Services;

use JWTAuth;
use App\Helpers\Error;
use App\Repositories\ProjectRepository;
use App\Repositories\ReleaseRepository;

class ReleaseService extends Service
{
	public function __construct()
	{
		$this->repository = new ReleaseRepository();
		$this->projectRepository = new ProjectRepository();

		$this->addRequiredPermission('listProjectReleases', ['list.releases']);
		$this->addRequiredPermission('createRelease', ['create.releases']);
		$this->addRequiredPermission('showRelease', ['list.releases']);
		$this->addRequiredPermission('updateRelease', ['update.releases']);
		$this->addRequiredPermission('deleteRelease', ['delete.releases']);

	}

	/**
	 * Allows the user to list the Releases that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @return
	 */
	protected function listReleases($user, $projectId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			return $project->releases()->paginate();
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to create the Release that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  array $data
	 * @return
	 */
	protected function createRelease($user, $projectId, $data)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$data['project_id'] = $project->id;
			return $this->repository->create($data);
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to show the Release that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @return
	 */
	protected function showRelease($user, $projectId, $releaseId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
			if ($release) {
				return $release;
			}
			else{
				return new Error('http-error', 'not-found',
					['release' => $releaseId], 'The resource was not found', 404);
			}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to update the Release that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @param  array $data
	 * @return
	 */
	protected function updateRelease($user, $projectId, $releaseId, $data)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
				if ($release) {
					return $this->repository->update($release, $data);
				} else {
					return new Error('http-error', 'not-found',
						['release' => $releaseId], 'The resource was not found', 404);
				}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to delete the Release that belong to a Project.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @param  array $data
	 * @return
	 */
	protected function deleteRelease($user, $projectId, $releaseId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
				if ($release) {
					return $this->repository->deleteById($release->id);
				} else {
					return new Error('http-error', 'not-found',
						['release' => $releaseId], 'The resource was not found', 404);
				}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

}
