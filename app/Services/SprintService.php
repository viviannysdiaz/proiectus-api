<?php

namespace App\Services;

use JWTAuth;
use App\Helpers\Error;
use App\Repositories\ProjectRepository;
use App\Repositories\SprintRepository;

class SprintService extends Service
{
	public function __construct()
	{
		$this->repository = new SprintRepository();
		$this->projectRepository = new ProjectRepository();

		$this->addRequiredPermission('listSprints', ['list.sprints']);
		$this->addRequiredPermission('createSprint', ['create.sprints']);
		$this->addRequiredPermission('showSprint', ['list.sprints']);
		$this->addRequiredPermission('updateSprint', ['update.sprints']);
		$this->addRequiredPermission('deleteSprint', ['delete.sprints']);

	}

	/**
	 * Allows the user to list the Sprints that belong to a Release.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @return
	 */
	protected function listSprints($user, $projectId, $releaseId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
			if ($release) {
				return $release->sprints()->paginate();
			} else {
				return new Error('http-error', 'not-found',
					['release' => $releaseId], 'The resource was not found', 404);
			}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to create the Sprints that belong to a Release.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @param  array $data
	 * @return
	 */
	protected function createSprint($user, $projectId, $releaseId, $data)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
			if ($release) {
				$data['release_id'] = $release->id;
				return $this->repository->create($data);
			} else {
				return new Error('http-error', 'not-found',
					['release' => $releaseId], 'The resource was not found', 404);
			}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to show the Sprint that belong to a Release.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @param  int $sprintId
	 * @return
	 */
	protected function showSprint($user, $projectId, $releaseId, $sprintId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
			if ($release) {
				$sprint = $release->sprints->find($sprintId);
				if ($sprint) {
					return $sprint;
				} else {
					return new Error('http-error', 'not-found',
						['sprint' => $sprintId], 'The resource was not found', 404);
				}
			}
			else{
				return new Error('http-error', 'not-found',
					['release' => $releaseId], 'The resource was not found', 404);
			}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to update the Sprint that belong to a Release.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @param  int $sprintId
	 * @param  array $data
	 * @return
	 */
	protected function updateSprint($user, $projectId, $releaseId, $sprintId, $data)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
				if ($release) {
					$sprint = $release->sprints->find($sprintId);
					if ($sprint) {
						return $this->repository->update($sprint, $data);
					} else {
						return new Error('http-error', 'not-found',
							['sprint' => $sprintId], 'The resource was not found', 404);
					}
				} else {
					return new Error('http-error', 'not-found',
						['release' => $releaseId], 'The resource was not found', 404);
				}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

	/**
	 * Allows the user to delete the Sprint that belong to a Release.
	 * @param  App\User $user
	 * @param  int $projectId
	 * @param  int $releaseId
	 * @param  int $sprintId
	 * @param  array $data
	 * @return
	 */
	protected function deleteSprint($user, $projectId, $releaseId, $sprintId)
	{
		$project = $this->projectRepository->findById($projectId);
		if ($project) {
			$release = $project->releases->find($releaseId);
				if ($release) {
					$sprint = $release->sprints->find($sprintId);
					if ($sprint) {
						return $this->repository->deleteById($sprint->id);
					} else {
						return new Error('http-error', 'not-found',
							['sprint' => $sprintId], 'The resource was not found', 404);
					}
				} else {
					return new Error('http-error', 'not-found',
						['release' => $releaseId], 'The resource was not found', 404);
				}
		} else {
			return new Error('http-error', 'not-found',
				['project' => $projectId], 'The resource was not found', 404);
		}
	}

}
