<?php

namespace App\Services;

use App\Helpers\Error;

class Service
{
	protected $repository;

	protected $realmLockedMethods = [];

	protected $requiredPermissions = [];

	public function setRealmLockedMethods($methodArray)
	{
		$this->realmLockedMethods = $methodArray;
	}

	public function addRequiredPermission($method, array $permissions)
	{
		return $this->requiredPermissions[$method] = $permissions;
	}

	public function authorize($user, $method)
	{
		$approvedPermissions = 0;

		if (count($this->requiredPermissions) == 0) {
			return true;
		}

		if (isset($this->requiredPermissions[$method])) {
			foreach ($this->requiredPermissions[$method] as $permissionSlug) {
				if ($user->can($permissionSlug)) {
					$approvedPermissions += 1;
				}
			}

			if (count($this->requiredPermissions[$method]) <> $approvedPermissions) {
				return false;
			}

			return true;
		}

		return true;
	}

	public function __call($method, $arguments)
	{
		$user = $arguments[0];

		if (!$this->authorize($user, $method)) {
			return new Error('permissions-error', 'unauthorized', $this->requiredPermissions[$method],
				'The user can\'t access the requested operation', 401);
		}

		return call_user_func_array(array($this, $method), $arguments);
	}
}
