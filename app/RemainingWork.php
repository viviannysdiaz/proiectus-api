<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemainingWork extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'remaining_works';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activity_id',
		'day',
		'remaining_work',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Relationships.
     *
     */
    public function activity()
    {
        return $this->belongsTo('App\Activity');
    }
}
