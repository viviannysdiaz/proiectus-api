<?php

namespace App\Helpers\Validators;

use App\Helpers\Validators\BaseValidator;

class UserStoryValidator extends BaseValidator
{

    protected function rules($id)
    {
        return [
            'description' => 'string|required'
        ];
    }
}



