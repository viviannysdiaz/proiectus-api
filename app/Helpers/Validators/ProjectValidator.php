<?php

namespace App\Helpers\Validators;

use App\Helpers\Validators\BaseValidator;

class ProjectValidator extends BaseValidator
{

    protected function rules($id)
    {
        return [
            'product_owner_id' => 'exists:users,id|required',
            'scrum_master_id' => 'exists:users,id|required',
            'name' => 'required|unique:projects,name'.$id,
            'description' => 'required'
        ];
    }
}



