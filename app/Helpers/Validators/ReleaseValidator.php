<?php

namespace App\Helpers\Validators;

use App\Helpers\Validators\BaseValidator;

class ReleaseValidator extends BaseValidator
{

    protected function rules($id)
    {
        return [
            'objetive' => 'required|string|max:250'.$id,
            'description' => 'required|string|max:500'.$id
        ];
    }

}
