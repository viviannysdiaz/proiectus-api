<?php

namespace App\Helpers\Validators;

use App\Helpers\Validators\BaseValidator;

class UserValidator extends BaseValidator
{

    protected function rules($id)
    {
        return [
            'name' => 'sometimes|required|max:150',
            'username' => 'required|string|unique:users,username'.$id,
            'email' => 'required|email|unique:users,email'.$id,
            'password' => 'required|min:6'
        ];
    }
}
