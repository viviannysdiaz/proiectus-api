<?php

namespace App\Helpers\Validators;

use App\Helpers\Validators\BaseValidator;

class ActivityValidator extends BaseValidator
{

    protected function rules($id)
    {
        return [
            'sprint_id' => 'required|exists:sprints,id',
            'developer_id' => 'required|exists:developers,id',
            'user_story_id' => 'required|exists:user_stories,id',
            'start_date' => 'required|date',
            'finish_date' => 'required|date|after:start_date'
        ];
    }
}





