<?php

namespace App\Helpers\Validators;

use App\Helpers\Validators\BaseValidator;

class SprintValidator extends BaseValidator
{

    protected function rules($id)
    {
        return [
            'name' => 'required|string',
            'objetive' => 'required|string',
            'start_date' => 'required|date',
            'finish_date' => 'required|date|after:start_date'
        ];
    }
}







