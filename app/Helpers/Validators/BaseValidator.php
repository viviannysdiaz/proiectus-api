<?php

namespace App\Helpers\Validators;

use Validator;

class BaseValidator
{
    protected $errors;

    public function __construct(array $data = null)
    {
        if ($data) {
            return $this->validate($data);
        }
    }

    public function validate(array $data)
    {
        if ($this->validator($data)->fails()) {
            $this->errors = $this->validator($data)->getMessageBag();
            return false;
        }

        return true;
    }

    /**
     * return the errors detected by the validator.
     *
     * @return array
     */
    public function errors()
    {
        return $this->errors ? $this->errors->toArray() : null;
    }

    /**
     * Removes required rules from rules array.
     * The rule should be always the first one.
     *
     * @param  array $rulesArray
     * @param  int $id
     * @return array
     */
    protected function makeRules($rulesArray, $id)
    {
        if ($id != ''){
            return array_map(function ($rule) {
                return str_replace('required|', '', $rule);
            }, $rulesArray);
        }
        return $rulesArray;
    }

    /**
     * return a Validator class instance.
     *
     * @param  array  $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        $id = array_key_exists('id', $data) ? ','.$data['id']  : "";
        return Validator::make($data, $this->makeRules($this->rules($id), $id));
    }
}
