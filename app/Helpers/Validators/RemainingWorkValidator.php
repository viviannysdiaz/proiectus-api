<?php

namespace App\Helpers\Validators;

use App\Helpers\Validators\BaseValidator;

class RemainingWorkValidator extends BaseValidator
{

    protected function rules($id)
    {
        return [
			'day' => 'required',
			'remaining_work' => 'required'
        ];
    }
}