<?php

namespace App\Helpers;

/**
 * This class is used to describe and catch errors in the aplication
 *
 */
class Error
{
    /**
     * Describes the category of the error.
     *
     * @var string
     */
    private $type;

    /**
     * A unique string identifier for the error.
     *
     * @var string
     */
    private $code;

    /**
     * The parameters that triggered the error (if theres any).
     *
     * @var Array
     */
    private $params;

    /**
     * A descriptive message to inform the user or log the error.
     *
     * @var string
     */
    private $message;

    /**
     * A numeric code, in the majority of the cases its gonna be the same as the related HTTP status code.
     * @var Int
     */
    private $statusCode;

    public function __construct($type = null, $code = null, $params = [], $message = null, $statusCode = null)
    {
        $this->type = $type;
        $this->code = $code;
        $this->params = $params;
        $this->message = $message;
        $this->statusCode = $statusCode;
    }

    public function appendParam($param)
    {
        return array_push($this->params, $param);
    }

    public function __get($property)
    {
      if (property_exists($this, $property)) {
        return $this->$property;
      }
      return null;
    }

    public function __set($property, $value)
    {
      if (property_exists($this, $property)) {
        $this->$property = $value;
      }
      return null;
    }

    public function __toString()
    {
       return $this->toJson();
    }

    public function toJson()
    {
        return json_encode($this->toArray(), 0);
    }

    public function toArray()
    {
        $array = array();
        foreach ($this as $key => $value) {
            $array[$key] = $value;
        }

        return $array;
    }
}