<?php

namespace App\Helpers;

use JWTAuth;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;

trait TokenDecoder
{
    public function getUserFromToken($token = null)
    {
        if (is_bool(JWTAuth::getToken())) {
            return null;
        }

        if(!$token) {
            $token = JWTAuth::getToken()->get();
        }

        $user_id = JWTAuth::getPayload($token)->get('sub');
        $user = User::findOrFail($user_id);

        return $user ?: null;
    }
}
