<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sprint_id',
        'user_story_id',
		'user_id',
        'status',
        'priority',
        'description',
        'estimated_hours',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

     /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'remaining_hours',
    ];

    /**
     * Relationships.
     *
     */
    public function sprint()
    {
        return $this->belongsTo('App\Sprint');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function remainingWorks()
    {
        return $this->hasMany('App\RemainingWork');
    }

    /**
     * Accesors
     *
     */
    public function getRemainingHoursAttribute()
    {
        return $this->remainingWorks()->orderBy('id', 'DESC')->first()->remaining_work;
    }
}
