<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_stories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'release_id',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

     /**
     * Relationships.
     *
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function release()
    {
        return $this->belongsTo('App\Release');
    }
}
