<?php

namespace App\Exceptions;

use Exception;

/**
 * ResourceNotFoundException.
 *
 *
 */
class ResourceNotFoundException extends Exception
{
    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'Resource Not Found',
        \Exception $previous = null, $code = 404)
    {
        parent::__construct($message, $code, $previous);
    }
}
