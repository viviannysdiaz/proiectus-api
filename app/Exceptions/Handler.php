<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Helpers\Error;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $error = new Error('http-error', 'not-found', [], 'The resource was not found', 404);
            return response($error, 404)->header('Content-Type', 'appplication/json');
        } elseif ($e instanceof NotFoundHttpException) {
            $error = new Error('http-error', 'not-found', [], 'The resource was not found', 404);
            return response($error, 404)->header('Content-Type', 'appplication/json');
        } elseif ($e instanceof BadRequestException) {
            $error = new Error('bad-request', 'not-found', [], $e->getMessage(), $e->getCode());
            return response($error, 404)->header('Content-Type', 'appplication/json');
        } elseif ($e instanceof \Tymon\JWTAuth\Exceptions\JWTException) {
            $error = new Error('bad-request', 'not-found', [], $e->getMessage(), 400);
            return response($error, 400)->header('Content-Type', 'appplication/json');
        }

        return parent::render($request, $e);
    }
}
