<?php

namespace App\Exceptions;

use Exception;

/**
 * ResourceConflictException.
 *
 *
 */
class ResourceConflictException extends Exception
{
    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'Resource Conflict',
        \Exception $previous = null, $code = 409)
    {
        parent::__construct($message, $code, $previous);
    }
}
