<?php

namespace App\Exceptions;

use Exception;

/**
 * BadRequestException.
 *
 *
 */
class BadRequestException extends Exception
{
    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'Bad Request',
        \Exception $previous = null, $code = 400)
    {
        parent::__construct($message, $code, $previous);
    }
}
