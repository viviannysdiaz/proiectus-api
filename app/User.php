<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Added
 */
use Hash;
use Johnnymn\Sim\Roles\Traits\HasRoleAndPermission;
use Johnnymn\Sim\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Model implements AuthenticatableContract,
                                    HasRoleAndPermissionContract,
                                    CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, HasRoleAndPermission;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'avatar',
        'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'superuser',
        'developer_count',
        'scrum_master_count'
    ];

    /**
     * Mutator to hash the password before saving
     *
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Relationships.
     *
     */
    public function owner()
    {
        return $this->hasMany('App\Project', 'product_owner_id');
    }

    public function scrumMaster()
    {
        return $this->hasMany('App\Project', 'scrum_master_id');
    }

    public function developers()
    {
        return $this->belongsToMany('App\Project', 'developers');
    }

    public function activities()
    {
        return $this->hasMany('App\Activities');
    }

    /**
     * Accesors
     */
    public function getDeveloperCountAttribute()
    {
        return $this->belongsToMany('App\Project', 'developers')->count();
    }

    public function getScrumMasterCountAttribute()
    {
        return $this->hasMany('App\Project', 'scrum_master_id')->count();
    }

    public function getSuperuserAttribute()
    {
        if ($this->is('superuser')) {
            return true;
        }
        return false;
    }
}
