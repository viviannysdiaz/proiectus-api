<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes
Route::post('auth/login',  'Auth\AuthController@authenticateUser');

// Protected Routes
Route::group(['middleware' => ['auth', 'format.response']], function () {

	Route::post('token/refresh', 'Auth\AuthController@refreshToken');

	Route::post('auth/user', 'Auth\AuthController@registerUser');

	// Users Routes
	Route::get('users/{userId}/projects', 'ProjectsController@getUserProjectsByRole');
	Route::resource('users', 'UsersController');

	// Projects CRUD Routes
	Route::get('projects', 'ProjectsController@index');
	Route::post('projects', 'ProjectsController@store');
	Route::get('projects/{projectId}', 'ProjectsController@show');
	Route::put('projects/{projectId}', 'ProjectsController@update');
	Route::delete('projects/{projectId}', 'ProjectsController@destroy');

	// User Histories CRUD
	Route::get('projects/{projectId}/user-histories', 'UserStoriesController@index');
	Route::post('projects/{projectId}/user-histories', 'UserStoriesController@store');
	Route::get('projects/{projectId}/user-histories/{userStoryId}', 'UserStoriesController@show');
	Route::put('projects/{projectId}/user-histories/{userStoryId}', 'UserStoriesController@update');
	Route::delete('projects/{projectId}/user-histories/{userStoryId}', 'UserStoriesController@destroy');

	// Releases CRUD
	Route::get('projects/{projectId}/releases', 'ReleasesController@index');
	Route::post('projects/{projectId}/releases', 'ReleasesController@store');
	Route::get('projects/{projectId}/releases/{releaseId}', 'ReleasesController@show');
	Route::put('projects/{projectId}/releases/{releaseId}', 'ReleasesController@update');
	Route::delete('projects/{projectId}/releases/{releaseId}', 'ReleasesController@destroy');

	// Sprints CRUD
	Route::get('projects/{projectId}/releases/{releaseId}/sprints', 'SprintsController@index');
	Route::post('projects/{projectId}/releases/{releaseId}/sprints', 'SprintsController@store');
	Route::get('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}', 'SprintsController@show');
	Route::put('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}', 'SprintsController@update');
	Route::delete('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}', 'SprintsController@destroy');

	// Sprint Burndown Chart
	Route::get('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}/burndownchart', 'SprintsController@burndownchart');

	// Activities CRUD
	Route::get('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}/activities', 'ActivitiesController@index');
	Route::post('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}/activities', 'ActivitiesController@store');
	Route::get('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}/activities/{activityId}', 'ActivitiesController@show');
	Route::put('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}/activities/{activityId}', 'ActivitiesController@update');
	Route::delete('projects/{projectId}/releases/{releaseId}/sprints/{sprintId}/activities/{activityId}', 'ActivitiesController@destroy');

	// Projects - Activities
	Route::get('projects/{projectId}/activities', 'ProjectsController@getCurrentActivities');

});