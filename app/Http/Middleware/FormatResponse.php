<?php

namespace App\Http\Middleware;

use Closure;
use Response;

use App\Helpers\Error;

class FormatResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (property_exists($response, 'original')) {
            if ($response->original instanceof Error) {
                return Response::make($response->original, $response->original->statusCode, $response->headers->all())
                    ->header('Content-Type', 'application/json');
            } else {
                if (is_array($response->original)) {
                    $content = $response->original;
                } elseif (is_string($response->original)) {
                    $content = $response->original;
                } elseif (is_integer($response->original)) {
                    $content = $response->original;
                } else {
                    $content = $response->original->toArray();
                }
            }
        }

        $response = Response::make($content, 200, $response->headers->all())
            ->header('Content-Type', 'application/json');

        return $response;
    }
}
