<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use App\Exceptions\BadRequestException;
use App\Exceptions\UnauthorizedException;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$token = JWTAuth::getToken()) {
            throw new BadRequestException('Missing Token');
        }

        if (!$token = JWTAuth::decode($token)) {
            throw new UnauthorizedException('Invalid Token');
        }

        return $next($request);
    }
}
