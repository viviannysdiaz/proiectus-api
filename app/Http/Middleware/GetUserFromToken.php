<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\TokenDecoder;


class GetUserFromToken
{
    use TokenDecoder;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = $this->getUserFromToken()) {
            $request->user = $user;
            return $next($request);
        }

        return $next($request);
    }
}
