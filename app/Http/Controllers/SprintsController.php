<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\SprintService;
use App\Services\RemainingWorkService;

class SprintsController extends Controller
{
    /**
     * Constructor of the class.
     *
     */
    public function __construct()
    {
        $this->service = new SprintService();
        $this->remainingWorkService = new RemainingWorkService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($projectId, $releaseId, Request $request)
    {
        return $this->service->listSprints($request->user, $projectId, $releaseId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($projectId, $releaseId, Request $request)
    {
        return $this->service->createSprint($request->user, $projectId, $releaseId, $request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($projectId, $releaseId, $sprintId, Request $request)
    {
        return $this->service->showSprint($request->user, $projectId, $releaseId, $sprintId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($projectId, $releaseId, $sprintId, Request $request)
    {
        return $this->service->updateSprint($request->user, $projectId, $releaseId, $sprintId, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($projectId, $releaseId, $sprintId, Request $request)
    {
        return $this->service->deleteSprint($request->user, $projectId, $releaseId, $sprintId);
    }

    /**
     * Get the info for the burndownchart of the sprint.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function burndownchart($projectId, $releaseId, $sprintId, Request $request)
    {
        return $this->remainingWorkService->burnDownChartInfo($request->user, $projectId, $releaseId, $sprintId);
    }
}
