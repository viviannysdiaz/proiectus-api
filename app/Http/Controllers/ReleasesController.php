<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\ReleaseService;

class ReleasesController extends Controller
{
    /**
     * Constructor of the class.
     *
     */
    public function __construct()
    {
        $this->service = new ReleaseService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($proyectId, Request $request)
    {
        return $this->service->listReleases($request->user, $proyectId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($proyectId, Request $request)
    {
        return $this->service->createRelease($request->user, $proyectId, $request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($proyectId, $releaseId, Request $request)
    {
        return $this->service->showRelease($request->user, $proyectId, $releaseId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($proyectId, $releaseId, Request $request)
    {
        return $this->service->updateRelease($request->user, $proyectId, $releaseId, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($proyectId, $releaseId, Request $request)
    {
        return $this->service->deleteRelease($request->user, $proyectId, $releaseId);
    }
}
