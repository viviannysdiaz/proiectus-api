<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\ProjectService;

class ProjectsController extends Controller
{
    /**
     * Constructor of the class.
     *
     */
    public function __construct()
    {
        $this->service = new ProjectService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ?: 15;
        return $this->service->listProject($request->user, $limit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->service->createProject($request->user, $request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $projectId
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $projectId)
    {
        return $this->service->showProject($request->user, $projectId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $projectId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $projectId)
    {
        return $this->service->updateProject($request->user, $projectId, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $projectId)
    {
        return $this->service->deleteProject($request->user, $projectId);
    }

    /**
     * Get the projects that are associated to the user.
     * @param  Request $request
     * @param  int  $userId
     * @return \Illuminate\Http\Response
     */
    public function getUserProjectsByRole(Request $request, $userId)
    {
        return $this->service->getUserProjectsByRole($request->user, $userId);
    }

    /**
     * Get the activities registered for the current srpint of the project.
     * @param  Request $request   [description]
     * @param  int  $projectId [description]
     * @return \Illuminate\Http\Response
     */
    public function getCurrentActivities(Request $request, $projectId)
    {
        return $this->service->getCurrentActivities($request->user, $projectId, $request->status);
    }
}
