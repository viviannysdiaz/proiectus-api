<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\UserStoryService;

class UserStoriesController extends Controller
{
    /**
     * Constructor of the class.
     *
     */
    public function __construct()
    {
        $this->service = new UserStoryService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($proyectId, Request $request)
    {
        return $this->service->listUserStories($request->user, $proyectId, $request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($proyectId, Request $request)
    {
        return $this->service->createUserStory($request->user, $proyectId, $request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($proyectId, $userStoryId, Request $request)
    {
        return $this->service->showUserStory($request->user, $proyectId, $userStoryId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($proyectId, $userStoryId, Request $request)
    {
        return $this->service->updateUserStory($request->user, $proyectId, $userStoryId, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($proyectId, $userStoryId, Request $request)
    {
        return $this->service->deleteUserStory($request->user, $proyectId, $userStoryId);
    }
}
