<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->service = new UserService();
        $this->middleware('format.response', ['only' => ['authenticateUser']]);
    }

    public function authenticateUser(Request $request)
    {
        return $this->service->authenticateUser($request->all());
    }

    public function registerUser(Request $request)
    {
        return $this->service->registerUser($request->user, $request->all());
    }

    public function refreshToken(Request $request)
    {
        return $this->service->refreshToken($request->user);
    }
}
