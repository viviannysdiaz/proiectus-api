<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sprint extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sprints';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'release_id',
        'number',
        'name',
        'objetive',
        'days',
        'ideal_work',
        'start_date',
        'finish_date',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'activities_count',
    ];

    /**
     * Relationships.
     *
     */
    public function release()
    {
        return $this->belongsTo('App\Release');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    /**
     * Accesors
     */
    public function getActivitiesCountAttribute()
    {
        return $this->activities()->count();
    }
}
