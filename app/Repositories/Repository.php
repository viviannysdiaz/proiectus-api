<?php

namespace App\Repositories;

use App\Helpers\Error;

class Repository
{
	protected $class;

	protected $validator;

	public function __construct($model, $validator = null)
	{
		$this->class = new $model();
		$this->validator = $validator != null ? $validator :
			$this->getValidatorFromClass($model);
	}

	public function create($data)
	{
		if (!$this->validator->validate($data)) {
			return new Error('validation-error', 'invalid-data',
				$this->validator->errors(), 'The input parameters were invalid', 401);
		}
		return $this->class->create($data);
	}

	public function findById($id)
	{
		return $this->class->find($id);
	}

	public function getAll($limit)
	{
		return $this->class->paginate($limit);
	}

	public function update($entity, $data)
	{
		if (!$this->validator->validate(array_merge($data, ['id' => $entity->id]))) {
			return new Error('validation-error', 'invalid-data',
				$this->validator->errors(), 'The input parameters were invalid', 401);
		}

		$entity->fill($data)->save();
		return $entity;
	}

	public function deleteById($id)
	{
		return $this->class->destroy($id);
	}

	public function batchDelete($idArray)
	{
		return $this->class->destroy($idArray);
	}

	private function getValidatorFromClass($model)
	{
		$className = get_class($model);

		$validatorClassName = 'App\Helpers\Validators\\'.
			explode("\\", $className)[1]. 'Validator';
		$validatorClass = new $validatorClassName;

		return $validatorClass;
	}
}
