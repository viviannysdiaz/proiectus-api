<?php

namespace App\Repositories;

use App\Activity;

class ActivityRepository extends Repository
{
	public function __construct()
	{
		parent::__construct(new Activity);
	}

	public function getActivitiesByStatus($sprintId, $status)
	{
		return Activity::where('sprint_id', $sprintId)->where('status', $status)->get();
	}
}
