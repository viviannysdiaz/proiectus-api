<?php

namespace App\Repositories;

use App\Release;

class ReleaseRepository extends Repository
{
	public function __construct()
	{
		parent::__construct(new Release);
	}
}
