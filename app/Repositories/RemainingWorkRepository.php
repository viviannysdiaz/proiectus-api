<?php

namespace App\Repositories;

use App\RemainingWork;

class RemainingWorkRepository extends Repository
{
	public function __construct()
	{
		parent::__construct(new RemainingWork);
	}

	public function getRemainingWorkByDay($activityIds, $day)
	{
		return RemainingWork::whereIn('activity_id', $activityIds)->where('day', $day)->sum('remaining_work');
	}

}