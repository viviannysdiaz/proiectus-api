<?php

namespace App\Repositories;

use App\Sprint;

class SprintRepository extends Repository
{
	public function __construct()
	{
		parent::__construct(new Sprint);
	}
}
