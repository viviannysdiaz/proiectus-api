<?php

namespace App\Repositories;

use App\Project;
use App\Activity;
use Carbon\Carbon;

class ProjectRepository extends Repository
{
	public function __construct()
	{
		parent::__construct(new Project);
	}

	public function getProjectList($limit)
	{
		$projects = Project::paginate($limit);
		return $projects;
	}

	public function getUserProjects($user, $limit)
	{
		$projects = Project::where('product_owner_id', $user->id)
			->orWhere('scrum_master_id', $user->id)
			->orWhereHas('developers', function($q) use ($user) {
				$q->where('developers.user_id', $user->id);
			})->paginate($limit);

		return $projects;

	}

	public function getCurrentActivities($project, $status)
	{
		$current_release = $project->releases()->max('number');
		$release = $project->releases()->where('number', $current_release)->first();
		if ($release) {
			$current_sprint = $release->sprints()->max('number');
			$sprint = $release->sprints()->where('number', $current_sprint)->first();
			if ($sprint) {
				return $sprint->activities()->where('status', $status)->with(['user', 'sprint'])->paginate();
			}
		}
	}
}
