<?php

namespace App\Repositories;

use App\UserStory;

class UserStoryRepository extends Repository
{
	public function __construct()
	{
		parent::__construct(new UserStory);
	}
}
