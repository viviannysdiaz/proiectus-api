<?php

namespace App\Repositories;

use Auth;
use App\User;
use App\Helpers\Error;
use Johnnymn\Sim\Roles\Models\Role;
use Validator;

class UserRepository extends Repository
{
	public function __construct()
	{
		parent::__construct(new User);
	}

	public function authenticateUser($data)
    {
        $validator = $this->validate($data);
        if($validator->fails()){
            return new Error('validation-error', 'invalid-data',
                $validator->errors(), 'The input parameters were invalid', 401);
        }
        $authUser = Auth::once($data);
        if ($authUser) {
            return Auth::user();
        } else {
        return new Error('authentication-error', 'credentials-missmatch'
                , ['email', 'password'], 'The credentials don\'t match', 401);
        }
    }

    public function getUserRolesByLevel($user)
    {
        return $user->roles()->orderBy('level', 'desc')
            ->select('roles.id', 'roles.name', 'roles.slug')->get();
    }

    public function validate($data)
    {
        return $validator = Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
    }

    public function getUsersList($limit)
    {
        return $users = User::paginate($limit);
    }

}
